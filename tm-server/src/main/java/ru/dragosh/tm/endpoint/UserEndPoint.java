package ru.dragosh.tm.endpoint;

import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;
import ru.dragosh.tm.exception.EntityNotExistsException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserEndPoint {
    @WebMethod
    Session authorisation(Session session, String login, String password) throws AccessForbiddenException, EntityIsAlreadyExistException, EntityNotExistsException;
    @WebMethod
    Session registration(Session session, String login, String password) throws AccessForbiddenException, EntityIsAlreadyExistException;
    @WebMethod
    Session updateLogin(Session session, String newLogin) throws AccessForbiddenException, EntityIsAlreadyExistException;
    @WebMethod
    Session updatePassword(Session session, String newPassword) throws AccessForbiddenException;
    @WebMethod
    User getProfile(Session session) throws AccessForbiddenException;
    @WebMethod
    Session logout(Session session) throws Exception;
}
