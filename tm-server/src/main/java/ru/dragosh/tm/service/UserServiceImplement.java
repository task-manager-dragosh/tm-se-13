package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.TaskRepository;
import ru.dragosh.tm.repository.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.MyBatisUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserServiceImplement implements UserService {
    @Nullable
    @Override
    public User find(@NotNull final String login,
                     @NotNull final String password) {
        if (login == null || login.isEmpty())
            return null;
        if (password == null || password.isEmpty())
            return null;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        return userRepository.find(login, password);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty())
            return null;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        return userRepository.findByLogin(login);
    }

    @Override
    public User findById(@NotNull final String id) {
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        return userRepository.findById(id);
    }

    @Override
    public void persist(@NotNull final User user) {
        if (user == null)
            return;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        userRepository.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (user == null)
            return;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        userRepository.merge(user);
    }

    @Override
    public void remove(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        userRepository.remove(userId);
    }

    @NotNull
    @Override
    public List<User> getEntitiesList() {
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        return userRepository.getEntitiesList();
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities) {
        if (entities == null)
            return;
        UserRepository userRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(UserRepository.class);
        for (User user: entities) {
            userRepository.persist(user);
        }
    }
}
