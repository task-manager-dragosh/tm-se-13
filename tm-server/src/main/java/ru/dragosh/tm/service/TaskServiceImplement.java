package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.SessionRepository;
import ru.dragosh.tm.repository.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.util.MyBatisUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

public final class TaskServiceImplement implements TaskService {
    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId,
                              @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.findAll(userId, projectId);
    }

    @Nullable
    @Override
    public Task find(@NotNull final String userId,
                     @NotNull final String projectId,
                     @NotNull final String nameTask) {
        if (userId == null || userId.isEmpty())
            return null;
        if (projectId == null || projectId.isEmpty())
            return null;
        if (nameTask == null || nameTask.isEmpty())
            return null;
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.find(userId, projectId, nameTask);
    }

    @Override
    public void persist(@NotNull final Task task) throws ParseException {
        if (task == null)
            return;
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        taskRepository.persist(task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (task == null)
            return;
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        taskRepository.merge(task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String taskId) {
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        taskRepository.remove(userId, taskId);
    }

    @Override
    public void removeAll(@NotNull final String userId,
                          @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return;
        if (projectId == null || projectId.isEmpty())
            return;
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        taskRepository.removeAll(userId, projectId);
    }

    @Override
    public List<Task> getEntitiesList() {
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.getEntitiesList();
    }

    @Override
    public void loadEntities(@NotNull final List<Task> entities) throws ParseException {
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        for (Task task: entities) {
            taskRepository.persist(task);
        }
    }

    @NotNull
    @Override
    public List<Task> findByStringPart(@NotNull final String userId,
                                       @NotNull final String projectId,
                                       @NotNull final String str) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.findByStringPart(userId, projectId, str);
    }

    @NotNull
    @Override
    public List<Task> getSortedBySystemTime(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.getSortedBySystemTime(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateStart(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.getSortedByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateFinish(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.getSortedByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByStatus(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        TaskRepository taskRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(TaskRepository.class);
        return taskRepository.getSortedByStatus(userId);
    }
}
