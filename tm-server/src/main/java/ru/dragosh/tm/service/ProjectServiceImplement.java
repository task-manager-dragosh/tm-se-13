package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.repository.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.util.MyBatisUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectServiceImplement implements ProjectService {
    @Override
    public void persist(@NotNull final Project entity) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateFinish())));
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        projectRepository.persist(entity);
    }

    @Override
    public void merge(@NotNull final Project entity) {
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        projectRepository.merge(entity);
    }

    @Override
    public void remove(@NotNull final String userId,
                       @NotNull final String entityId) {
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        projectRepository.remove(userId, entityId);
    }

    @NotNull
    @Override
    public List<Project> getEntitiesList() {
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.getEntitiesList();
    }

    @Override
    public void loadEntities(@NotNull final List<Project> entities) throws ParseException {
        final ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        for (Project project: entities) {
            projectRepository.persist(project);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project find(@NotNull final String projectName,
                        @NotNull final String userId) {
        if (projectName == null || projectName.isEmpty())
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.find(projectName, userId);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        projectRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public List<Project> findByStringPart(@NotNull final String userId,
                                          @NotNull final String str) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.findByStringPart(userId, str);
    }

    @NotNull
    @Override
    public List<Project> getSortedBySystemTime(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                                                .getSqlSessionFactory()
                                                .openSession()
                                                .getMapper(ProjectRepository.class);
        return projectRepository.getSortedBySystemTime(userId);
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateStart(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.getSortedByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateFinish(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.getSortedByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Project> getSortedByStatus(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        ProjectRepository projectRepository = MyBatisUtil
                .getSqlSessionFactory()
                .openSession()
                .getMapper(ProjectRepository.class);
        return projectRepository.getSortedByStatus(userId);
    }
}
