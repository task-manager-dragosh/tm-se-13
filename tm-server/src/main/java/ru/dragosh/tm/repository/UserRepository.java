package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository {
    @Nullable
    @Select("select * from app_user where login = #{login} and hash_password = #{password}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "hash_password"),
            @Result(property = "role", column = "role_id")
    })
    User find(String login, String password);

    @Nullable
    @Select("select * from app_user where login = #{login}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "hash_password"),
            @Result(property = "role", column = "role_id")
    })
    User findByLogin(String login);

    @Nullable
    @Select("select * from app_user where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "hash_password"),
            @Result(property = "role", column = "role_id")
    })
    User findById(String id);

    @Insert("insert into app_user (#{id}, #{login}, #{password}, #{role})")
    void persist(User user);

    @Update("update app_user id = #{id}, login = #{login}, password = #{password}, role = #{role}")
    void merge(User user);

    @Delete("delete from app_user where user_id = #{userId}")
    void remove(String userId);

    @Nullable
    @Select("select * from app_user")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "hash_password"),
            @Result(property = "role", column = "role_id")
    })
    List<User> getEntitiesList();
}
