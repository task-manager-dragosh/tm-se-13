package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface SessionRepository {
    @Nullable
    @Select("select * from session")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "timeStamp", column = "time_stamp"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
    })
    List<Session> findAll();

    @Nullable
    @Select("select * from session where user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "timeStamp", column = "time_stamp"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
    })
    Session findOne(@NotNull String userId) throws Exception;

    @Insert("insert into session (#{id}, #{timeStamp}, #{signature}, #{userId})")
    void persist(@NotNull Session session) throws EntityIsAlreadyExistException, AccessForbiddenException;

    @Update("update session set id = #{id}, time_stamp = #{timeStamp}, signature = #{signature}, user_id = #{userId} where id = #{sessionId}")
    void merge(@NotNull String sessionId, @NotNull Session session);

    @Delete("delete from session where id = #{sessionId}")
    void remove(@NotNull String sessionId) throws Exception;

    @Delete("delete from session")
    void removeAll() throws Exception;
}
