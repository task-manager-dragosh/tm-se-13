package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Task;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface TaskRepository extends Repository<Task> {
    @Nullable
    @Select("select * from task user_id = #{userId} and project_id = #{project_id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    List<Task> findAll(String userId, String projectId);

    @Nullable
    @Select("select * from task user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    Task find(String userId, String projectId, String nameTask);

    @Delete("delete from task user_id = #{userId} and project_id = #{projectId}")
    void removeAll(String userId, String projectId);

    @Nullable
    @Select("select * from task user_id = #{userId} and project_id = #{projectId} and regexp_like(task_name, #{str}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    List<Task> findByStringPart(String userId, String projectId, String str);

    @Insert("insert into task (#{id}, #{taskName}, #{description}, #{dateStart}, #{dateFinish}, #{projectId}, #{userId}, #{systemTime}, #{status})")
    @Override
    void persist(Task entity) throws ParseException;

    @Update("update task set id = #{id}, task_name = #{taskName}, description = #{description}, date_start = #{dateStart}, date_finish = #{dateFinish}, project_id = #{projectId}, user_id = #{userId}, system_time = #{systemTime}")
    @Override
    void merge(Task entity);

    @Delete("delete from task where user_id = #{userId} and id = #{entityId}")
    @Override
    void remove(String userId, String entityId);

    @Nullable
    @Select("select * from task")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Task> getEntitiesList();

    @Nullable
    @Select("select * from task user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Task> getSortedBySystemTime(String userId);

    @Nullable
    @Select("select * from task user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Task> getSortedByDateStart(String userId);

    @Nullable
    @Select("select * from task user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Task> getSortedByDateFinish(String userId);

    @Nullable
    @Select("select * from task user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "task_name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Task> getSortedByStatus(String userId);
}