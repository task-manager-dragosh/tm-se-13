package ru.dragosh.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Project;

import javax.annotation.RegEx;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ProjectRepository extends Repository<Project> {
    @Nullable
    @Select("select * from project where user_id = #{userId}")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "description", column = "description"),
        @Result(property = "dateStart", column = "date_start"),
        @Result(property = "dateFinish", column = "date_finish"),
        @Result(property = "name", column = "name"),
        @Result(property = "userId", column = "user_id"),
        @Result(property = "status", column = "status_id"),
        @Result(property = "systemTime", column = "system_time")
    })
    List<Project> findAll(String userId);

    @Select("select * from project where user_id = #{userId} and name = #{projectName}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    Project find(String projectName, String userId);

    @Delete("delete from project where user_id = #{userId}")
    void removeAll(String userId);

    @Select("select * from project where user_id = #{userId} and regexp_like(project_name, #{str})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    List<Project> findByStringPart(String userId, String str);

    @Insert("insert into project values (#{id}, #{project_name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{status}, #{systemTime})")
    @Override
    void persist(Project entity) throws ParseException;

    @Update("update project set name = #{project_name}, description = #{description}, date_start = #{dateStart}, date_finish = #{dateFinish}, user_id = #{userId}, system_time = #{systemTime}, status_id = #{status}")
    @Override
    void merge(Project entity);

    @Delete("delete from project where user_id = #{userId} and id = #{entityId}")
    @Override
    void remove(String userId, String entityId);

    @Nullable
    @Select("select * from project")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "project_name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Project> getEntitiesList();

    @Nullable
    @Select("select * from project where user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "project_name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Project> getSortedBySystemTime(String userId);

    @Nullable
    @Select("select * from project where user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Project> getSortedByDateStart(String userId);

    @Nullable
    @Select("select * from project where user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "project_name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Project> getSortedByDateFinish(String userId);

    @Nullable
    @Select("select * from project where user_id = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "name", column = "project_name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status_id"),
            @Result(property = "systemTime", column = "system_time")
    })
    @Override
    List<Project> getSortedByStatus(String userId);
}