package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.entity.containters.TaskList;
import ru.dragosh.tm.exception.Exception_Exception;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "TaskEndPoint", targetNamespace = "http://endpoint.tm.dragosh.ru/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TaskEndPoint {
    @WebMethod
    TaskList findAll(@WebParam(name = "session", partName = "arg0") Session session,
                     @WebParam(name = "session", partName = "arg1") String projectId);
    @WebMethod
    Task find(@WebParam(name = "session", partName = "arg0") Session session,
              @WebParam(name = "projectId", partName = "arg1") String projectId,
              @WebParam(name = "taskName", partName = "arg2")String taskName) throws Exception_Exception;
    @WebMethod
    void removeAll(@WebParam(name = "session", partName = "arg0") Session session,
                   @WebParam(name = "projectId", partName = "arg1")String projectId);
    @WebMethod
    TaskList findByStringPart(@WebParam(name = "session", partName = "arg0") Session session,
                              @WebParam(name = "projectId", partName = "arg1")String projectId,
                              @WebParam(name = "part", partName = "arg2")String part);
    @WebMethod
    void persist(@WebParam(name = "task", partName = "arg0") Task Task) throws Exception_Exception;
    @WebMethod
    void merge(@WebParam(name = "task", partName = "arg0") Task task);
    @WebMethod
    void remove(@WebParam(name = "session", partName = "arg0") Session session,
                @WebParam(name = "taskId", partName = "arg0") String taskId);
    @WebMethod
    TaskList getSortedBySystemTime(@WebParam(name = "session", partName = "arg0") Session session);
    @WebMethod
    TaskList getSortedByDateStart(@WebParam(name = "session", partName = "arg0") Session session);
    @WebMethod
    TaskList getSortedByDateFinish(@WebParam(name = "session", partName = "arg0") Session session);
    @WebMethod
    TaskList getSortedByStatus(@WebParam(name = "session", partName = "arg0") Session session);
}
