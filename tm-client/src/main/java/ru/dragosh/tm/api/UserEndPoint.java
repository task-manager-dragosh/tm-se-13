package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "UserEndPoint", targetNamespace = "http://endpoint.tm.dragosh.ru/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserEndPoint {
    @WebMethod
    Session authorisation(@WebParam(name = "session", partName = "arg0") Session session,
                          @WebParam(name = "login", partName = "arg1") String login,
                          @WebParam(name = "password", partName = "arg2") String password);
    @WebMethod
    Session registration(@WebParam(name = "session", partName = "arg0") Session session,
                         @WebParam(name = "login", partName = "arg1") String login,
                         @WebParam(name = "password", partName = "arg3") String password);
    @WebMethod
    Session updateLogin(@WebParam(name = "session", partName = "arg0") Session session,
                        @WebParam(name = "newLogin", partName = "arg1")String newLogin);
    @WebMethod
    Session updatePassword(@WebParam(name = "session", partName = "arg0") Session session,
                           @WebParam(name = "newPassword", partName = "arg1")String newPassword);
    @WebMethod
    User getProfile(@WebParam(name = "session", partName = "arg0") Session session);
    @WebMethod
    Session logout(@WebParam(name = "session", partName = "arg0") Session session);
}
