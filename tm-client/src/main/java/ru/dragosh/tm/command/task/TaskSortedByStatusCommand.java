package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.TaskEndPoint;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.endpoint.service.TaskEndPointService;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

public final class TaskSortedByStatusCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "sort tasks by status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(вывод задач отсортированных по статусу)";
    }

    @Override
    public void execute() {
        TaskEndPointService taskEndPointService = serviceLocator.getTaskEndPointService();
        TaskEndPoint taskEndPoint = taskEndPointService.getTaskEndPointPort();
        Session session = serviceLocator.getCurrentSession();

        taskEndPoint.getSortedByStatus(session);
    }
}
